#pragma warning(disable : 4514 4710)
#pragma warning(push)
#pragma warning(disable : 4820 4986)

#include <iostream>
using std::cout;
using std::endl;

#include <string>
using std::string;

#include <algorithm>
using std::transform;

#pragma warning(pop)

int main(int argc, char** argv)
{
  string commandLine = "rmdir /s";
  bool quiet = false;

  for(int i = 1; i < argc; ++i)
  {
    string arg = argv[i];

    if(arg == "/Y" || arg == "/y")
    {
      if(quiet)
      {
        continue;
      }

      commandLine.append(" /q");
      quiet = true;
    }
    else
    {
      commandLine.append(" ");
      commandLine.append(arg);
    }
  }

  system(commandLine.c_str());
}
